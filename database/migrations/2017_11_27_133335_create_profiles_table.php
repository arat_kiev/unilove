<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('it_name');
            $table->string('lastname');
            $table->string('it_lastname');
            $table->text('about');
            $table->text('it_about');
            $table->string('email');
            $table->string('phone');
            $table->string('skype');
            $table->date('birth');
            $table->string('zodiac');
            $table->string('it_zodiac');
            $table->string('hair_color');
            $table->string('it_hair_color');
            $table->string('eyes_color');
            $table->string('it_eyes_color');
            $table->string('education');
            $table->string('it_education');
            $table->boolean('alcohol');
            $table->boolean('smoking');
            $table->boolean('children');
            $table->integer('country_id');
            $table->integer('city_id');
            $table->string('photo');
            $table->string('photo_thumb');
            $table->text('video');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles');
    }
}
