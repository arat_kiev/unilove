<?php

/**
 * @var \SleepingOwl\Admin\Contracts\Navigation\NavigationInterface $navigation
 * @see http://sleepingowladmin.ru/docs/menu_configuration
 */

use SleepingOwl\Admin\Navigation\Page;

$navigation->setFromArray([
    [
        'title' => 'Пользователи',
        'icon' => 'fa fa-users',
        'priority' =>'1',
        'pages' => [
            (new Page(\App\User::class))
                ->setIcon('fa fa-users')
                ->addBadge(function () {
                    return \App\User::count();
                }, ['class' => 'label-success'])
                ->setPriority(0),
            (new Page(\App\Permission::class))
                ->setIcon('fa fa-lock')
                ->setPriority(1),
            (new Page(\App\Role::class))
                ->setIcon('fa fa-lock')
                ->setPriority(2)
        ]
    ],
    [
        'title' => 'Анкеты',
        'icon' => 'fa fa-id-card',
        'priority' =>'0',
        'pages' => [
            (new Page(\App\Profile::class))
                ->setIcon('fa fa-id-card')
                ->addBadge(function () {
                    return \App\Profile::count();
                }, ['class' => 'label-primary'])
                ->setPriority(0)
        ]
    ],
]);
