<?php

namespace Admin\Policies;

use Admin\Http\Sections\Roles;
use App\Models\User;
use App\Models\Role;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolesSectionModelPolicy
{

    use HandlesAuthorization;

    /**
     * @param User $user
     * @param string $ability
     * @param Roles $section
     * @param Role $item
     *
     * @return bool
     */
    public function before(User $user, $ability, Roles $section, Role $item)
    {
        if ($user->hasRole('admin')) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param Roles $section
     * @param Role $item
     *
     * @return bool
     */
    public function display(User $user, Roles $section, Role $item)
    {
        if ($user->hasRole('admin')) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param Blog $section
     * @param BlogEntry $item
     *
     * @return bool
     */
    public function create(User $user, Blog $section, BlogEntry $item)
    {
        return ($user->hasRole('admin') || ($user->hasRole('blogger')));
    }

    /**
     * @param User $user
     * @param Roles $section
     * @param Role $item
     *
     * @return bool
     */
    public function edit(User $user, Roles $section, Role $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Roles $section
     * @param Role $item
     *
     * @return bool
     */
    public function delete(User $user, Roles $section, Role $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Roles $section
     * @param Role $item
     *
     * @return bool
     */
    public function restore(User $user, Roles $section, Role $item)
    {
        return true;
    }
}
