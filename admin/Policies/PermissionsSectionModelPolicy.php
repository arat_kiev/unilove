<?php

namespace Admin\Policies;

use Admin\Http\Sections\Permissions;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionsSectionModelPolicy
{

    use HandlesAuthorization;

    /**
     * @param User $user
     * @param string $ability
     * @param Permissions $section
     * @param Permission $item
     *
     * @return bool
     */
    public function before(User $user, $ability, Permissions $section, Permission $item)
    {
        if ($user->hasRole('admin')) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param Permissions $section
     * @param Permission $item
     *
     * @return bool
     */
    public function display(User $user, Permissions $section, Permission $item)
    {
        if ($user->hasRole('admin')) {
            return true;
        }
    }

    /**
     * @param User $user
     * @param Blog $section
     * @param BlogEntry $item
     *
     * @return bool
     */
    public function create(User $user, Blog $section, BlogEntry $item)
    {
        return ($user->hasRole('admin') || ($user->hasRole('blogger')));
    }

    /**
     * @param User $user
     * @param Permissions $section
     * @param Permission $item
     *
     * @return bool
     */
    public function edit(User $user, Permissions $section, Permission $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Permissions $section
     * @param Permission $item
     *
     * @return bool
     */
    public function delete(User $user, Permissions $section, Permission $item)
    {
        return true;
    }

    /**
     * @param User $user
     * @param Permissions $section
     * @param Permission $item
     *
     * @return bool
     */
    public function restore(User $user, Permissions $section, Permission $item)
    {
        return true;
    }
}
