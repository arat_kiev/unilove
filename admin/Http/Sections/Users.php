<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use App\Role;
use App\Permission;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Contracts\Initializable;

/**
 * Class Users
 *
 * @property \App\User $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Users extends Section implements Initializable

{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Пользователи';

    public function initialize()
    {

    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('roles')
            ->with('abilities');

        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('Username')->setOperator('contains'),
            AdminColumnFilter::text()->setPlaceholder('Email')->setOperator('contains'),
            AdminColumnFilter::text()->setPlaceholder('Role')->setOperator('contains'),
            AdminColumnFilter::text()->setPlaceholder('Abilities')->setOperator('contains'),
        ])->setPlacement('panel.buttons');

        $display->setColumns([
            AdminColumn::link('username', 'Username'),
            AdminColumn::email('email', 'Email'),
            AdminColumn::lists('roles.display_name', 'Roles'),
            AdminColumn::lists('abilities.display_name', 'Abilities'),
            AdminColumn::custom('Avatar', function ($model) {
                return view('admin::partials.avatar', ['avatar' => config('app.image_storage.profile') . $model->getImage]);
            })->setWidth('50px'),
            AdminColumn::custom('Status', function($model) {
                switch ($model->active) {
                    case true:
                        $visibilityStatus = '<span class="label label-success">Active</span>';
                        break;
                    case false:
                        $visibilityStatus = '<span class="label label-danger">Disabled</span>';
                        break;
                    default:
                        $visibilityStatus = '<span class="label label-warning">N/A</span>';
                }
                return $visibilityStatus;
            }),
            AdminColumn::datetime('created_at', 'Register date')->setFormat('d.m.Y, H:i'),
        ])->paginate(15);
        $display->setOrder([[6, 'desc']]);

        return $display;
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('username', 'Username')->required(),
            AdminFormElement::text('email', 'E-mail')->required()->addValidationRule('email'),
            AdminFormElement::multiselect('roles', 'Roles', Role::class)->setDisplay('display_name'),
            AdminFormElement::multiselect('abilities', 'Abilities', Permission::class)->setDisplay('display_name'),
            AdminFormElement::checkbox('active', 'Active')
        ]);
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
