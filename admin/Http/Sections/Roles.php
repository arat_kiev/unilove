<?php

namespace Admin\Http\Sections;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use AdminColumnFilter;
use AdminColumnEditable;
use App\Models\Permission;
use SleepingOwl\Admin\Contracts\Display\DisplayInterface;
use SleepingOwl\Admin\Contracts\Form\FormInterface;
use SleepingOwl\Admin\Section;

/**
 * Class Roles
 *
 * @property \App\Role $model
 *
 * @see http://sleepingowladmin.ru/docs/model_configuration_section
 */
class Roles extends Section
{
    /**
     * @see http://sleepingowladmin.ru/docs/model_configuration#ограничение-прав-доступа
     *
     * @var bool
     */
    protected $checkAccess = false;

    /**
     * @var string
     */
    protected $title = 'Роли';

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        $display = AdminDisplay::datatablesAsync()
            ->with('permission')
            ->setColumns([
                AdminColumnEditable::text('display_name', 'Role'),
                AdminColumnEditable::text('name', 'Slug'),
                AdminColumn::lists('permission.display_name', 'Permissions')->setWidth('200px'),
                AdminColumn::text('description', 'Description'),
                AdminColumn::datetime('created_at', 'Created at')->setFormat('d.m.Y, H:i'),
            ])->paginate(10);

        $display->setColumnFilters([
            AdminColumnFilter::text()->setPlaceholder('Role')->setOperator('contains'),
            AdminColumnFilter::text()->setPlaceholder('Slug')->setOperator('contains'),
        ])->setPlacement('panel.buttons');

        $display->setOrder([[0, 'asc']]);

        return $display;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        return AdminForm::panel()->addBody([
            AdminFormElement::text('display_name', 'Name')->required(),
            AdminFormElement::text('name', 'Slug')->required(),
            AdminFormElement::textarea('description', 'Description'),
            AdminFormElement::multiselect('permission', 'Permissions', Permission::class)->setDisplay('display_name')
        ]);
    }

    /**
     * @return void
     */
    public function onDelete($id)
    {
        // todo: remove if unused
    }

    /**
     * @return void
     */
    public function onRestore($id)
    {
        // todo: remove if unused
    }
}
